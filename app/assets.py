from flask_assets import Bundle, Environment

bundles = {
    "main_js": Bundle(
        "js/main.js",
        output="build/main.js",
        filters="jsmin",
    ),
}


def register_bundles(app):
    assets = Environment(app)
    assets.register(bundles)
