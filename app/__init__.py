import os

from flask import Flask

from app.assets import register_bundles
from app.db import close_db
from app.inventory import inventory_blueprint
from app.rest_api import rest_api_blueprint


def create_app():
    # create and configure the app
    app = Flask(__name__)

    DEFAULT_KEY = "CHANGE_ME"
    app.config.from_mapping(
        DATABASE=os.path.join(app.instance_path, "clothingventory.sqlite"),
        SECRET_KEY=DEFAULT_KEY,
    )

    app.config.from_envvar("CLOTHINGVENTORY_CONFIG", silent=True)

    if app.config["ENV"] == "production" and app.config["SECRET_KEY"] == DEFAULT_KEY:
        # print colored warning
        print("\033[93m" + "WARNING: Using default secret key!" + "\033[0m")

    # ensure the instance folder exists
    try:
        os.makedirs(app.instance_path)
    except FileExistsError:
        pass

    app.register_blueprint(inventory_blueprint)
    app.register_blueprint(rest_api_blueprint)
    register_bundles(app)
    app.teardown_appcontext(close_db)

    return app
