import aiohttp
import asyncio
from app.constants import PRODUCT_TYPES, BASE_URL, PRODUCTS_URL, AVAILABILITY_URL
from app.db import save_products, delete_products


async def fetch_product_info(product_type):
    async with aiohttp.ClientSession() as session:
        async with session.get(PRODUCTS_URL + product_type) as resp:
            return (product_type, await resp.json())


async def fetch_availability_info(manufacturer):
    def parse_datapayload(payload: str):
        """parse html-string from original availability API"""
        before = payload.find("<INSTOCKVALUE>") + len("<INSTOCKVALUE>")
        after = payload.find("</INSTOCKVALUE>")
        return payload[before:after]

    while True:
        async with aiohttp.ClientSession() as session:
            async with session.get(AVAILABILITY_URL + manufacturer) as resp:
                data = await resp.json()

                # in error case (response is string) re-try fetching
                if type(data["response"]) is list:
                    availability_info = {
                        obj["id"].lower(): parse_datapayload(obj["DATAPAYLOAD"])
                        for obj in data["response"]
                    }
                    return (manufacturer, availability_info)


async def fetch_all_info(db):
    """Fetch all info on all known product types and save data to database"""
    product_infos = []
    manufacturers = set()
    availability_infos = {}

    # Fetching Product Info
    product_info_fetchers = [fetch_product_info(p) for p in PRODUCT_TYPES]
    product_infos = await asyncio.gather(*product_info_fetchers)

    # Collecting Manufacturer Names
    for product_info in product_infos:
        for product in product_info[1]:
            manufacturers.add(product["manufacturer"])

    # Fetching Availability Info
    availability_info_fetchers = [fetch_availability_info(m) for m in manufacturers]
    availability_infos = await asyncio.gather(*availability_info_fetchers)
    availability_infos = {m: a for (m, a) in availability_infos}

    for product_info in product_infos:
        product_type = product_info[0]
        products = product_info[1]
        for p in products:
            manufacturer = p["manufacturer"]
            id = p["id"]
            p["availability"] = availability_infos[manufacturer][id]
        delete_products(product_type, db)
        save_products(product_type, products, db)
