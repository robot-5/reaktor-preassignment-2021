from flask import Blueprint, render_template

from app.constants import PRODUCT_TYPES, REST_API_PATH

inventory_blueprint = Blueprint("inventory", __name__)


@inventory_blueprint.route("/")
def inventory_view():
    return render_template(
        "inventory/view.html",
        product_types=PRODUCT_TYPES,
        rest_api_path=REST_API_PATH,
    )
