import sqlite3

from flask import current_app, g
from app.constants import PRODUCT_TYPES


def get_db():
    if "db" not in g:
        g.db = sqlite3.connect(
            current_app.config["DATABASE"], detect_types=sqlite3.PARSE_DECLTYPES
        )
        g.db.row_factory = sqlite3.Row

    return g.db


def db_is_set_up(db):
    """Check if database is setup correctly and ready to use"""

    table_rows = db.execute(
        "SELECT name FROM sqlite_master WHERE type='table'"
    ).fetchall()
    tables = [t["name"] for t in table_rows]
    return sorted(tables) == sorted(PRODUCT_TYPES)


def setup_db(db):
    """Clear the existing data and create new tables."""

    script = """
    DROP TABLE IF EXISTS {product_type};
    CREATE TABLE {product_type} (
    id TEXT PRIMARY KEY,
    name TEXT,
    color TEXT,
    price INTEGER,
    manufacturer TEXT,
    availability TEXT);"""

    for p in PRODUCT_TYPES:
        db.executescript(script.format(product_type=p))


def close_db(e=None):
    db = g.pop("db", None)
    if db:
        db.close()


def save_products(product_type, products, db=None):
    if not db:
        db = get_db()
    for i in range(len(products)):
        id = products[i]["id"]
        name = products[i]["name"]
        color = ",".join(products[i]["color"])
        price = products[i]["price"]
        manufacturer = products[i]["manufacturer"]
        availability = products[i]["availability"]
        db.execute(
            f"INSERT INTO {product_type} (id, name, color, price, manufacturer, availability) VALUES (?, ?, ?, ?, ?, ?)",
            (id, name, color, price, manufacturer, availability),
        )
    db.commit()


def delete_products(product_type, db=None):
    if not db:
        db = get_db()
    db.execute(f"DELETE FROM {product_type}")
    db.commit()


def load_products(product_type):
    def product_row_to_dict(p):
        return {
            "id": p["id"],
            "name": p["name"],
            "color": p["color"].split(","),
            "price": p["price"],
            "manufacturer": p["manufacturer"],
            "availability": p["availability"],
        }

    db = get_db()
    products = db.execute(
        "SELECT id, name, color, price, manufacturer, availability"
        " FROM {}".format(product_type)
    ).fetchall()

    return [product_row_to_dict(p) for p in products]
