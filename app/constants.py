"""
Constant values for the business logic
"""

BASE_URL = "https://bad-api-assignment.reaktor.com/v2/"
PRODUCTS_URL = f"{BASE_URL}products/"
AVAILABILITY_URL = f"{BASE_URL}availability/"

PRODUCT_TYPES = ["beanies", "facemasks", "gloves"]
REST_API_PATH = "/api/v1/products/"
