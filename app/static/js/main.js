/**
 * Fetch product information from server or from session storage if up-to-date
 * @param {string} category 
 */
async function fetch_products(category) {
    const target_url = new URL(category, window.PRODUCTS_URL);
    const headers = {
        'Content-Type': 'application/json',
    };

    const saved_hash = window.sessionStorage.getItem(category + "_hash");
    if (saved_hash !== null) {
        headers['If-None-Match'] = saved_hash;
    }

    const resp = await fetch(target_url, { headers });

    switch (resp.status) {
        case 200:
            const text = await resp.text()
            window.sessionStorage.setItem(category, text);
            window.sessionStorage.setItem(category + "_hash", resp.headers.get("Etag"));
            return await JSON.parse(text);
        case 304:
            const saved_products = window.sessionStorage.getItem(category);
            return JSON.parse(saved_products);
        default:
            console.warn("Got Unexpected server response!");
            return [];
    }
}


class ProductTable {

    static productKeys = [
        "id",
        "name",
        "color",
        "price",
        "manufacturer",
        "availability",
    ];

    // human-friendly text and bootstrap styling for availability values
    static availabilityInfo = {
        INSTOCK: ["In Stock", "table-success"],
        LESSTHAN10: ["<10", "table-warning"],
        OUTOFSTOCK: ["No Stock", "table-danger"],
    };

    tbodyHtmlElement;
    titleHtmlElement;
    currentProducts = {
        category: "",
        products: [],
        showingCount: 0,
        reset: function () { this.category = ""; this.products = [], this.showingCount = 0; }
    };
    batchSize = 1000;
    intersectionObserver;

    constructor(tableID = "product-table", titleID = "product-table-title") {
        this.tbodyHtmlElement = document.getElementById(tableID).getElementsByTagName("tbody")[0];
        this.titleHtmlElement = document.getElementById(titleID);

        // when user scrolls to last table row, display more products
        let callback = (entries, observer) => {
            entries.forEach(entry => {
                if (entry.isIntersecting) {
                    observer.disconnect();
                    let howManyToShow = (this.currentProducts.products.length - this.currentProducts.showingCount);
                    if (howManyToShow > 0) {
                        howManyToShow = (howManyToShow > this.batchSize) ? this.batchSize : howManyToShow;
                        const from = this.currentProducts.showingCount;
                        const to = from + howManyToShow;
                        this.populateTable(this.currentProducts.products, from, to, false);
                    }
                }
            });
        };

        this.intersectionObserver = new IntersectionObserver(callback);
    }

    /**
     * Set title of product table
     * @param {string} category 
     * @param {number} amount 
     */
    setTitle(category, amount) {
        const capitalized = category.charAt(0).toUpperCase() + category.substring(1)
        this.titleHtmlElement.innerText = `${amount}\ ${capitalized}`;
    }


    /**
     * Populate table row with data from product
     * @param {HTMLTableRowElement} row 
     * @param {object} product 
     */
    static populateRow(index, row, product) {
        row.insertCell().appendChild(document.createTextNode(index + 1))
        ProductTable.productKeys.forEach((key) => {
            const cell = row.insertCell();
            let content = "";
            switch (key) {
                case "color":
                    content = product[key].join(", ");
                    break;
                case "availability":
                    const info = ProductTable.availabilityInfo[product[key]];
                    if (info) {
                        content = info[0];
                        row.classList.add(info[1]);
                    } else {
                        console.debug(`Got unexpected availability: ${product[key]}`);
                        content = product[key];
                    }
                    break;
                default:
                    content = product[key];
                    break;
            }
            const text = document.createTextNode(content);
            cell.appendChild(text);
        });

    }

    /**
     * Populate table with given product data
     * @param {object[]} products 
     */
    populateTable(products, from = 0, to = undefined, replace = true) {
        if (replace) {
            this.tbodyHtmlElement.innerHTML = '';
        }

        if (to == undefined) {
            to = products.length;
        }

        for (let i = from; i < to; ++i) {
            const product = products[i];
            const row = this.tbodyHtmlElement.insertRow();
            ProductTable.populateRow(i, row, product);
        }

        this.currentProducts.showingCount += (to - from);

        //observe when last element enters viewport
        const target = this.tbodyHtmlElement.rows[this.tbodyHtmlElement.rows.length - 1];
        this.intersectionObserver.observe(target);
    }

    /**
     * Display products of given category, replacing previously displayed products.
     * @param {string} category 
     */
    async show_products(category) {
        this.intersectionObserver.disconnect();
        this.currentProducts.reset();
        this.currentProducts.category = category;
        const opacityBefore = this.tbodyHtmlElement.style.opacity;
        this.tbodyHtmlElement.style.opacity = 0.3;

        const products = await fetch_products(category);
        this.currentProducts.products = products;
        this.setTitle(this.currentProducts.category, products.length);
        const howManyToShow = (products.length > this.batchSize) ? this.batchSize : products.length;
        this.populateTable(this.currentProducts.products, 0, howManyToShow);
        this.tbodyHtmlElement.style.opacity = opacityBefore;
    }
}


class ProductSelectButtonList {

    buttonHtmlElements;

    constructor(elementID = "product-select-buttons") {
        this.buttonHtmlElements = document.getElementById(elementID).getElementsByTagName("input");
    }

    /**
     * Enable or disable all buttons
     * @param {bool} value 
     */
    setButtonsEnabled(value) {
        for (const button of this.buttonHtmlElements) {
            if (value) {
                button.removeAttribute("disabled");
            } else {
                button.disabled = true;
            }
        };
    }
}

/**
 * Create a Bootstrap spinner to indicate loading
 */
function createSpinner() {
    const internalSpan = document.createElement("span");
    internalSpan.setAttribute("class", "visually-hidden");
    internalSpan.textContent = "Loading...";

    const spinner = document.createElement("div");
    spinner.setAttribute("class", "spinner-border");
    spinner.setAttribute("role", "status");
    spinner.appendChild(internalSpan);
    return spinner;
}

// Add logic to switch between displayed product categories
document.addEventListener("DOMContentLoaded", function () {
    const productSelectButtons = new ProductSelectButtonList();
    const productTable = new ProductTable();

    // set logic for product category selection 
    for (const button of productSelectButtons.buttonHtmlElements) {
        button.onclick = async function () {

            window.scroll(
                {
                    top: 0,
                    left: 0,
                    behavior: "auto"
                });

            // indicate loading
            productSelectButtons.setButtonsEnabled(false);
            const spinner = createSpinner();
            document.getElementById("product-select-buttons").append(spinner);

            await productTable.show_products(this.value);

            // remove loading indication 
            spinner.remove();
            setTimeout(() => productSelectButtons.setButtonsEnabled(true), 0);
        };
    }

    // fetch initial product info
    productSelectButtons.buttonHtmlElements[0]?.click();
});