from flask import Blueprint, jsonify, request, make_response
from hashlib import sha256
from http import HTTPStatus
from app.constants import PRODUCT_TYPES, REST_API_PATH
from app.db import load_products


rest_api_blueprint = Blueprint("rest_api", __name__, url_prefix=REST_API_PATH)


@rest_api_blueprint.route("/<product_type>")
def products_endpoint(product_type):

    if product_type not in PRODUCT_TYPES:
        return ("Unknown Product Type", 404)

    etags = request.headers.get("If-None-Match", default=[])
    if etags:
        etags = etags.split(", ")

    products = load_products(product_type)
    json_resp = jsonify(products)
    product_hash = sha256(json_resp.get_data()).hexdigest()

    if product_hash in etags:
        resp = make_response("", HTTPStatus.NOT_MODIFIED)
    else:
        resp = json_resp

    resp.headers["Etag"] = product_hash
    return resp