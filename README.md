# __Clothingventory__
This is my web application __Clothingventory__ (working title) for the [Reaktor Pre-Assignment 2021](https://www.reaktor.com/junior-dev-assignment):
It collects information about clothes inventory and provides the data to warehouse workers.
A script `periodic_fetching.py` is used to retrieve the data every few seconds from the bad-api-assignment.reaktor.com API and save it to an SQLite database.
A [gunicorn](https://gunicorn.org/) server delivers a [Flask](https://flask.palletsprojects.com/en/1.1.x/) web app which both provides a (minimalistic) frontend and a JSON API which will be queried by the frontend.
Since this a rather small use-case with a fixed scope (as there will be no changes made to the used API), the frontend is implemented in plain JavaScript.

__Note:__ This app was developed on Ubuntu 20.04 with Python version 3.8.

## Installation
To run the app locally, first download and `cd` to this repository.
The app uses a default secret key, which is good enough for local testing, but _must_ be replaced for deployment.
If you want to do so, create a `config.py` file, e.g. in the repository, containing a secret key of your choice:
```python
SECRET_KEY = "SOME_LONG_RANDOM_STRING"
```
Then set an environment variable pointing to your config file:
```bash
export CLOTHINGVENTORY_CONFIG=$(pwd)/config.py
```

With either the default or an individual secret key, you can install and start the app as follows:
```bash
python3 -m venv venv
. venv/bin/activate
pip install -r requirements.txt

export FLASK_ENV="production"

python3 periodic_fetching.py &
gunicorn -b 0.0.0.0:8080 "app:create_app()"
```

## Usage
Now you can visit the app at http://localhost:8080 and the API endpoints at http://localhost:8080/api/v1/products/PRODUCTNAME.
On the very first startup, 
it will take about 15 to 20 seconds until any data is available.
This is due to the response times of the endpoints under bad-api-assignment.reaktor.com.
The database and logs from `periodic_fetching.py` can be found in the `instance` directory.

When you are done using the app, you can terminate like this:
```bash
# hit `Ctrl+C` to kill gunicorn
fg
# hit `Ctrl+C` to kill the `periodic_fetching.py` process
```

In a deployment, both the `periodic_fetching.py` and the `gunicorn` commands
should be registered as services, for example using `systemd`. 
