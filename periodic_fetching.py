"""
Periodically fetch products and availability data.
This should run constantly as one process (system-wide).
Register e.g. with systemd.
"""

import asyncio
import sqlite3
import os
from app import create_app
from app.data_fetching import fetch_all_info
from app.db import db_is_set_up, setup_db
import logging


def setup_logger(filepath):
    log_handler = logging.handlers.RotatingFileHandler(
        filepath, mode="a", maxBytes=(20 * 1024 * 1024), backupCount=3
    )
    formatter = logging.Formatter(
        f"%(asctime)s {__file__} [%(process)d]: %(message)s", "%b %d %H:%M:%S"
    )
    log_handler.setFormatter(formatter)

    logger = logging.getLogger(__file__)
    logger.addHandler(log_handler)
    logger.setLevel(logging.INFO)
    return logger


if __name__ == "__main__":
    APP = create_app()
    DB_LOCATION = APP.config["DATABASE"]
    logfile = os.path.join(APP.instance_path, f"{__file__}.log")
    LOG = setup_logger(logfile)

    LOG.info(f"Start fetching product data")
    while True:
        try:
            try:
                os.makedirs(os.path.split(DB_LOCATION)[0])
            except FileExistsError:
                pass

            db = sqlite3.connect(
                DB_LOCATION,
                detect_types=sqlite3.PARSE_DECLTYPES,
            )
            db.row_factory = sqlite3.Row

            if not db_is_set_up(db):
                setup_db(db)
            asyncio.run(fetch_all_info(db))
            db.close()
            LOG.info(f"Did save product data")
            asyncio.run(asyncio.sleep(5))
        except Exception as e:
            LOG.exception("Exception:", exc_info=e)
